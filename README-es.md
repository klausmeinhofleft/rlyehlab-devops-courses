# Cursos de Devops

### Temario

1.  ¿Que es DevOps?
    1.  ¿Dónde se usa?
    2.  ¿Cómo se implementa?
2.  Introducción a Linux
3.  Shell scripting
4.  DevSecOps
    1.  GPG
    2.  SSH
    3.  OpenSSL
5.  Herramienta de versionado
    1.  ¿Para qué se usa?
    2.  ¿Cuáles existen?
    3.  Introducción a git
6.  Contenedores
    1.  Docker
    2.  Docker-compose
7.  Integración Continua (CI) y Entrega Continua (CD)
8.  Automatización
    1.  Ansible
9.  Monitoreo
    1.  Alternativas

### Recursos

-   [Linux Journey](https://linuxjourney.com/)
-   [CodeCademy](https://www.codecademy.com/learn/learn-the-command-line)
-   [CodeCademy](https://www.codecademy.com/learn/learn-git)
-   [Servers for Hackers](https://serversforhackers.com/)
