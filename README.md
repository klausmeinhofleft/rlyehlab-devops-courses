# Devops Courses

### Syllabus

1.  What is DevOps?
    1.  Use Case
    2.  How to Implement
2.  Linux 101
3.  Shell Scripting
4.  DevSecOps
    1.  GPG
    2.  SSH
    3.  OpenSSL
5.  Versioning Tools
    2.  How many are there?
    3.  Use Case
    4.  Introduction to git
6.  Container
    1.  Docker
    2.  Docker-compose
7.  Continuous Integration (CI) & Continuous Delivery (CD)
8.  Automatization
    1.  Ansible
9.  Monitoring Tools
    1.  Alternatives

### Resources

-   [Linux Journey](https://linuxjourney.com/)
-   [CodeCademy](https://www.codecademy.com/learn/learn-the-command-line)
-   [CodeCademy](https://www.codecademy.com/learn/learn-git)
-   [Servers for Hackers](https://serversforhackers.com/)
